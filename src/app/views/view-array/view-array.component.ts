import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { DataList } from "../../dataList";
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

@Component({
  selector: 'app-view-array',
  templateUrl: './view-array.component.html',
  styleUrls: ['./view-array.component.css']
})
export class ViewArrayComponent implements OnInit {

  data : any = [];
  sortArray : any = [];
  dataFilter : any = [];
  frecArray : any = [];
  dataList : DataList[] = [];

  constructor(private api: ApiService, private slimLoadingBarService: SlimLoadingBarService) { }

  ngOnInit() {
  }

  carga() {

    this.startLoading();

    this.api.getDataArray()
    .subscribe(res => {
      this.dataList = [];
      this.data = res;

      console.log(this.data.data);

      //ordenamiento
      this.sortArray = this.data.data;
      this.sortArray = this.quickSort(this.sortArray);

      //sacar duplicados
      this.dataFilter = this.data.data;
      this.dataFilter = Array.from(new Set(this.dataFilter))

      //obtiene ocurrencias de numero procesado
      this.frecArray = this.data.data; 
      this.frecArray = this.frecuency (this.frecArray);

      //armar listado para pantalla
      for (let i = 0; i < this.dataFilter.length; i++) {
        const temp = this.dataFilter[i];
        let n = new DataList;        
        n.data = temp;
        n.quantity = Number(this.frecArray[temp]);
        n.first = this.data.data.indexOf(temp) + 1; //primera posicion en el arreglo original

        //ultima posicion en el arreglo original
        let ids = this.data.data.indexOf(temp);
        let last = ids;
        while (ids != -1) {
          ids = this.data.data.indexOf(temp, ids + 1);
          if (ids != -1) last = ids;
        }

        n.last = last + 1;

        this.dataList.push(n);
        this.completeLoading();
      }

    }, err => {
      console.log(err);
      this.completeLoading();
    });


  }

  frecuency(arr) {

    var counts = {};

    for (var i = 0; i < arr.length; i++) {
      var num = arr[i];
      counts[num] = counts[num] ? counts[num] + 1 : 1;
    }

    return counts;
}

  quickSort(array) {
    if (array.length < 2) {
      return array
    }
    const chosenIndex = array.length - 1
    const chosen = array[chosenIndex]
    const a = []
    const b = []
    for (let i = 0; i < chosenIndex; i++) {
      const temp = array[i]
      temp < chosen ? a.push(temp) : b.push(temp)
    }
  
    const output = [...this.quickSort(a), chosen, ...this.quickSort(b)]
    //console.log(output.join(' '))
    return output
  }

  getBackgroundColor(quantity) {

    if (quantity >= 2) {return "greenClass"}
    if (quantity == 1) {return "yellowClass"}
    if (quantity < 1) {return "greyClass"}
    
  }

  startLoading() {
    this.slimLoadingBarService.start();
  }

  completeLoading() {
      this.slimLoadingBarService.complete();
  }

}
