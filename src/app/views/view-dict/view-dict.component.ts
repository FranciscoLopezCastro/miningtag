import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

@Component({
  selector: 'app-view-dict',
  templateUrl: './view-dict.component.html',
  styleUrls: ['./view-dict.component.css']
})
export class ViewDictComponent implements OnInit {

  data : any = [];
  dataList : any = [];
  totales : any = [];
  arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

  constructor(private api: ApiService, private slimLoadingBarService: SlimLoadingBarService) { }

  ngOnInit() {
  }

  carga() {

    this.startLoading();

    this.api.getDataDict()
    .subscribe(res => {
      this.data = JSON.parse(res.data);

      for (let i = 0; i < this.data.length; i++) {
        let temp = this.data[i];
        let freq = this.getFrequency(temp.paragraph.toUpperCase());
        let suma = 0;

        for (let j = 0; j < this.arr.length; j++) {
          let a = this.arr[j];
          if (freq[a])
            suma += freq[a];          
        }
        this.totales.push(suma);
        this.dataList.push(freq);
      }

      this.completeLoading();
    }, err => {
      console.log(err);
      this.completeLoading();
    });


  }

  getFrequency(string) {
    var freq = {};
    for (var i=0; i<string.length;i++) {
        var character = string.charAt(i);
        if (freq[character]) {
           freq[character]++;
        } else {
           freq[character] = 1;
        }
    }

    return freq;
  }

  startLoading() {
    this.slimLoadingBarService.start();
  }

  completeLoading() {
      this.slimLoadingBarService.complete();
  }
}
