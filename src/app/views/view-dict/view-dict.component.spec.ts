import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDictComponent } from './view-dict.component';

describe('ViewDictComponent', () => {
  let component: ViewDictComponent;
  let fixture: ComponentFixture<ViewDictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
