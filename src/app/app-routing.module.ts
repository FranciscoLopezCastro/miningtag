import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewArrayComponent } from './views/view-array/view-array.component';
import { ViewDictComponent } from './views/view-dict/view-dict.component';

const routes: Routes = [
  {
    path: 'view-array',
    component: ViewArrayComponent,
    data: { title: 'Vista Array' }
  },
  {
    path: 'view-dict',
    component: ViewDictComponent,
    data: { title: 'Vista Dict' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
