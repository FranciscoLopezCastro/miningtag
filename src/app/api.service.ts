import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Data } from './data';

const apiUrlArray = "http://patovega.com/prueba_frontend/array.php";
const apiUrlDict = "http://patovega.com/prueba_frontend/dict.php";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getDataArray (): Observable<Data> {

    return this.http.get<Data>(apiUrlArray).pipe(
      catchError(err => {
          console.log(err);
          return of(null);
      })
      );
  }

  getDataDict (): Observable<Data> {

    return this.http.get<Data>(apiUrlDict).pipe(
      catchError(err => {
          console.log(err);
          return of(null);
      })
      );
  }

}
